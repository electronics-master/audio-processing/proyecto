#! /usr/bin/env python3

import argparse
import os
from sys import argv

from pyAudioAnalysis import audioTrainTest as aT

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Trains an audio classification model.')

    parser.add_argument("training_dir", help="Directory with labeled audio in separate folders", type=str)
    parser.add_argument("model", help="Model to be trained for classification", choices=["svm", "knn", "randomforest", "gradientboosting", "extratrees"], default="svm")
    parser.add_argument("model_name", help="Name of the model to be saved", type=str)

    args = parser.parse_args()

    subdirectories = os.listdir(args.training_dir)[:4]

    subdirectories = [args.training_dir + "/" + subDirName for subDirName in subdirectories]

    aT.extract_features_and_train(subdirectories, 1.0, 1.0, aT.shortTermWindow, aT.shortTermStep, args.model, args.model_name, False)