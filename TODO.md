# TODO

[] Instalar y conocer el sistema pyAudioAnalysis.
[] Seleccionar una fuente de podcasts costarricenses. Debe de tener segmentos de habla y música.
[] Hacer una segmentación manual para establecer el conjunto de entrenamiento de los clasificadores, es decir, separar en carpetas los segmentos de habla limpia y los de otros sonidos.
[] Usar pyAudioAnalysis, o bien otro sistema (como SoX), para separar automáticamente los audios en los puntos de silencio.
[] Usar los clasificadores de pyAudioAnalysis, entrenados con los datos del paso 3, para clasificador nuevos podcasts.
[] Evaluar los resultados a partir de una exploración manual de los resultados.
