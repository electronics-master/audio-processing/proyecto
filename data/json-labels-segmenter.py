#!/usr/bin/env python3

import argparse
import json
import os
import random
import sys

from scipy.io.wavfile import write
import numpy as np
import pydub 

def mp3_to_array(filename, normalized=False):
    """MP3 to numpy array"""
    audio = pydub.AudioSegment.from_mp3(filename)
    waveform = np.array(audio.get_array_of_samples())

    if audio.channels == 2:
        waveform = waveform.reshape((-1, 2))

    if normalized:
        return audio.frame_rate, np.float32(waveform) / 2**15
    else:
        return audio.frame_rate, waveform

def array_to_wav(filename, sample_rate, waveform, normalized=False):
    """numpy array to WAV"""
    channels = 2 if (waveform.ndim == 2 and waveform.shape[1] == 2) else 1

    if normalized:  # normalized array - each item should be a float in [-1, 1)
        waveform = np.int16(waveform * 2 ** 15)
    else:
        waveform = np.int16(waveform)

    write(filename, sample_rate, waveform.astype(np.int16))


def parse_json(json_string, train_test_split):
    """Parses the json and saves the segments as wav files

    Args:
        json_string (str): Json string with the label-studio format
    """
    json_object = json.loads(json_string)

    if not os.path.exists("test"):
        os.makedirs("test")
    if not os.path.exists("train"):
        os.makedirs("train")

    # Iterate over every labeled file
    for json_item in json_object:
        upload_filename = json_item["file_upload"]
        absolute_filename = ""

        # Try to find the labeled file
        for root, dirs, files in os.walk("."):
            if upload_filename in files:
                absolute_filename = os.path.join(root, upload_filename)

        if absolute_filename:
            filename, file_extension = os.path.splitext(upload_filename)
            if file_extension == ".mp3":
                sample_rate, waveform = mp3_to_array(absolute_filename)
            else:
                raise ValueError("Unsupported format: ", file_extension)
        else:
            continue

        # Only process further if there is at least one annotation
        if len(json_item["annotations"]) > 0:
            # Only consider the first annotation
            annotations = json_item["annotations"][0]["result"]

            # Convert each annotation into separate wav files
            for i, annotation in enumerate(annotations):

                # Set this as test or train
                if random.randint(0, 100) > train_test_split:
                    dataset = "train"
                else:
                    dataset = "test"

                start_sample = int(sample_rate * annotation["value"]["start"])
                end_sample = int(sample_rate * annotation["value"]["end"])
                label = annotation["value"]["labels"][0]

                output_waveform = waveform[start_sample:end_sample]
                output_filename = dataset + "/" + label + "/" + filename + "_" +  str(i) + ".wav"

                if len(output_waveform) == 0:
                    continue

                if not os.path.exists(dataset + "/" + label):
                    os.makedirs(dataset + "/" + label)

                array_to_wav(output_filename, sample_rate, output_waveform)

def range_limited_int_type(arg):
    """ Type function for argparse - a int within bounds """
    try:
        i = int(arg)
    except ValueError:    
        raise argparse.ArgumentTypeError("Must be an integer")
    if (i < 0) or (i > 100):
        raise argparse.ArgumentTypeError("Argument must be lesser than " + str(100) + " and larger than " + str(0))
    return i

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Reads a json file with the labels and separates the audio files accordingly.')

    parser.add_argument("json_filename", help="JSON filename where the data will be read", type=str)
    parser.add_argument("--train_test_split", "-s", help="Percentage of data that will be kept for testing data (pyAudioAnalisys also performs a split so this can be left as 0)", type=range_limited_int_type, default=0)

    args = parser.parse_args()
    
    with open(args.json_filename, "r") as json_file:
        parse_json(json_file.read(), args.train_test_split)

    
 