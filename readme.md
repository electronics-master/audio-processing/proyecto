# Sistema de Clasificacion de Audio

## Introducción

El propósito del proyecto es desarrollar un sistema que permita seleccionar segmentos de habla limpia de grabaciones de radio en vivo, o de podcasts. Esto es de suma importancia para el desarrollo de sistemas de reconocimiento, análisis o síntesis de voz propias, con nuestro acento y forma de hablar.

El sistema idealmente funciona de esta manera: Recibe un audio continuo, de larga duración (como un podcast de una hora), separa este podcasts en segmentos pequeños, identificando los silencios, y luego hace una selección de aquellos segmentos que corresponden a señales de habla pura, sin ruido de fondo o música. En este punto es entonces un sistema de clasificación.

### Objetivo general

Implementar un sistema de selección de segmentos de habla pura a partir de podcasts.

### Objetivos específicos:

* Valorar la importancia de contar con recursos de habla para el desarrollo de tecnologías propias.
* Desarrollar un conjunto de datos de prueba para el entrenamiento de clasificadores de audio en habla pura y otros sonidos.
* Implementar un sistema de segmentación de audios a partir de silencios.
* Implementar la clasificación de los sonidos segmentados usando algoritmos de clasificación convenientes.

## Metodología 

1. Exploración de podcast y programas de radios grabados con acentos costarricenses.
2. Etiquetación de segmentos de audio.
3. Extracción de los segmentos etiquetados hacia distintas carpetas.
4. Separación en un conjunto de datos de entrenamiento y uno de evaluación.
5. Entrenamiento de un modelo con PyAudioAnalisys.
6. Evaluación y iteración sobre distintos modelos.

## Herramientas


```
ffmpeg -i $INPUT_FILENAME -f segment -segment_time $SEGMENT_TIME_IN_SECS -c copy $OUTPUT_FILENAME_PREFIX%03d.mp3
```